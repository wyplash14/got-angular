import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { HousesComponent } from './components/houses/houses.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { CharactersComponent } from './components/characters/characters.component';
import { FormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    BooksComponent,
    CharactersComponent,
    AppComponent,
    HousesComponent,
    NavigationComponent
  ],
  imports: [
    MatSnackBarModule,
    MatExpansionModule,
    BrowserAnimationsModule, 
    FieldsetModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
