import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/model/book';
import { BooksService } from 'src/app/services/books/books.service';
import { CharactersService } from 'src/app/services/characters/characters.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent {
  public books: Book[] = [];
  searchTerm: string = "";
  searchBookTerm: string = "";
  searchCharacterTerm: string = "";
  openValue: boolean = false;
  LOAD_BOOK_NAME: string= "loadBookName";
  error: boolean = false;
  characterList: string[] = [];

  constructor(private charactersService: CharactersService, private booksService: BooksService, private route: ActivatedRoute, private elementRouter: Router){}

  ngOnInit(){
    this.route.params.subscribe((params)=>{
      if(params['name']){
        this.loadBookByName(params['name']);
        localStorage.setItem(this.LOAD_BOOK_NAME, params['name']);
        this.openValue = true;
      }else{
        var loadedBookName = localStorage.getItem(this.LOAD_BOOK_NAME);
        if(loadedBookName){
          this.elementRouter.navigate(['books', loadedBookName]);
        }else{
          this.openValue = false;
          this.getBooks();
        }
      } 
    });
  }

  getBooks(){
    this.booksService.getBooks().subscribe((books) =>{
      books.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfBook(value);   
      })
      this.books = books;
    });
  }

  setAtributesOfBook(value: Book){
    if(value.isbn == '') value.isbn = '(unknown)';
    if(value.publisher == '') value.publisher = '(unknown)';
    if(value.country == '') value.country = '(unknown)';
    if(value.mediaType == '') value.mediaType = '(unknown)';
    if(value.released == '') value.released='(unknown)';
    if(value.authors[0]=='') value.authors[0]='(unknown)';

    if(value.characters.length == 0) value.characters[0]='(unknown)';
    else{
      value.characters.forEach((url: string, index: number)=>{
        this.charactersService.getCharacterByUrl(url).subscribe((character)=>{
          value.characters[index] = character.name;
          this.characterList[index] = character.name;
        });
      });
    }
   
  }

  searchBookByName(name: string){
    if (name == '') {
      this.error=false;
      localStorage.setItem(this.LOAD_BOOK_NAME, '');
      this.elementRouter.navigate(['books']);
    }else{
      this.booksService.getBookByName(name).subscribe((books)=>{
          this.error=false;
        if(books.length > 0){
          books.forEach((value) => {
            if(value.name == '') value.name = "(unknown)"; 
            this.setAtributesOfBook(value);   
          })
          localStorage.setItem(this.LOAD_BOOK_NAME,name);
          this.elementRouter.navigate(['books', name]);
          this.books = books;
          this.searchTerm = name;
        }else
            this.error = true; 
      });
    }
  }

  loadBookByName(name: string){
    this.booksService.getBookByName(name).subscribe((books)=>{
     books.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfBook(value);   
      })
      this.books = books;
      this.searchTerm = name;
    });
  }

  clickOnCharacter(character: string){
    this.elementRouter.navigate(['characters', character]);
  }

  searchInList(name: string, book: Book){
    if(name){
      this.characterList = [];
      book.characters.forEach((character)=>{
        if(character.toLowerCase().includes(name.toLowerCase()))
          this.characterList.push(character);
        if(character.toLowerCase() == name.toLowerCase()){
          this.characterList = [];
          this.characterList.push(character);
        }
      })
    }else{
      this.characterList = book.characters;
    }
  }
}
