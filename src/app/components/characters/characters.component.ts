import { Component } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Character } from 'src/app/model/character';
import { BooksService } from 'src/app/services/books/books.service';
import { CharactersService } from 'src/app/services/characters/characters.service';
import { HousesService } from 'src/app/services/houses/houses.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent {
  public characters: Character[] = [];
  currentPage: number = 1;
  maxPages = 214;
  searchTerm: string = "";
  searchBookTerm: string = "";
  openValue: boolean = false;
  PAGE_NUMBER_CHARACTERS: string= "pageNumberCharacters";
  LOAD_CHARACTER_NAME: string ="loadCharacterName";
  error: boolean = false;
  bookList: string[] = [];

  constructor(private charactersService: CharactersService, private booksService: BooksService, private housesService: HousesService, private route: ActivatedRoute, private elementRouter: Router){}

  ngOnInit(){
    this.route.params.subscribe((params)=>{
      if(params['name']){
        this.loadCharacterByName(params['name']);
        localStorage.setItem(this.LOAD_CHARACTER_NAME, params['name']);
        this.openValue = true;
      }else{
        var loadedCharacterName = localStorage.getItem(this.LOAD_CHARACTER_NAME);
        if(loadedCharacterName){
          this.elementRouter.navigate(['characters', loadedCharacterName]);
        }else{
          this.openValue = false;
          this.currentPage = Number(localStorage.getItem(this.PAGE_NUMBER_CHARACTERS)) || 1;
          this.getCharacters(this.currentPage);
        }
      } 
    });
  }

  getCharacters(currentPage: number){
    this.charactersService.getCharacters(currentPage).subscribe((characters) =>{
      characters.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfCharacter(value);   
      })
      this.characters = characters;
    });
  }

  setAtributesOfCharacter(value: Character){
    if(value.culture == '') value.culture = '(unknown)';
    if(value.gender == '') value.gender = '(unknown)';
    if(value.born == '') value.born = '(unknown)';
    if(value.died == '') value.died = '(unknown)';
    if(value.titles[0] == '') value.titles[0]='(unknown)';


    if(value.father == '') value.father = '(unknown)';
    else this.charactersService.getCharacterByUrl(value.father).subscribe((father)=>{
          value.father = father.name; 
    });


    if(value.mother == '') value.mother = '(unknown)';
     else this.charactersService.getCharacterByUrl(value.mother).subscribe((mother)=>{
          value.mother = mother.name; 
    });

    if(value.spouse == '') value.spouse = '(unknown)';
    else this.charactersService.getCharacterByUrl(value.spouse).subscribe((spouse)=>{
          value.spouse = spouse.name; 
    });

    value.povBooks.forEach((url: string, index: number)=>{
      this.booksService.getBookByUrl(url).subscribe((book)=>{
        value.povBooks[index] = book.name;
        this.bookList[index] = book.name;
      });
    });

    if(value.allegiances.length == 0) value.allegiances[0]='(unknown)';
    else{
      value.allegiances.forEach((url: string, index: number)=>{
        this.housesService.getHouseByUrl(url).subscribe((house)=>{
          value.allegiances[index] = house.name;
        });
      });
    }

  }

  searchCharacterByName(name: string){
    if (name == '') {
      this.error=false;
      localStorage.setItem(this.LOAD_CHARACTER_NAME, '');
      this.elementRouter.navigate(['characters']);
    }else{
      this.charactersService.getCharacterByName(name).subscribe((characters)=>{
          this.error=false;
        if(characters.length > 0){
          characters.forEach((value) => {
            if(value.name == '') value.name = "(unknown)"; 
            this.setAtributesOfCharacter(value);   
          })
          localStorage.setItem(this.LOAD_CHARACTER_NAME,name);
          this.elementRouter.navigate(['characters', name]);
          this.characters = characters;
          this.searchTerm = name;
        }else
            this.error = true; 
      });
    }
  }

  loadCharacterByName(name: string){
    this.charactersService.getCharacterByName(name).subscribe((characters)=>{
      characters.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfCharacter(value);   
      })
      this.characters = characters;
      this.searchTerm = name;
    });
  }

  clickStepPage(currentPage: number){
    this.currentPage = currentPage;
    this.getCharacters(currentPage);
    localStorage.setItem(this.PAGE_NUMBER_CHARACTERS, currentPage.toString());
  }

  clickOnHouse(house: string){
    this.elementRouter.navigate(['houses', house]);
  }

  clickOnBooks(house: string){
    this.elementRouter.navigate(['books', house]);
  }

  searchInList(name: string, character: Character){
    if(name){
      this.bookList = [];
      character.povBooks.forEach((book)=>{
        if(book.toLowerCase().includes(name.toLowerCase()))
          this.bookList.push(book);
        if(book.toLowerCase() == name.toLowerCase()){
          this.bookList = [];
          this.bookList.push(book);
        }
      })
    }else{
      this.bookList = character.povBooks;
    }
  }
}
