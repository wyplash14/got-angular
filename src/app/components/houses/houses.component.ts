import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Character } from 'src/app/model/character';
import { House } from 'src/app/model/house';
import { CharactersService } from 'src/app/services/characters/characters.service';
import { HousesService } from 'src/app/services/houses/houses.service';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent {
  public houses: House[] = [];
  currentPage: number = 1;
  maxPages = 45;
  searchTerm: string = "";
  searchMemberTerm: string = "";
  LOAD_HOUSE_NAME: string = "loadHouseName";
  PAGE_NUMBER_HOUSES: string = "pageNumberHouses";
  openValue = false;
  swornList :string[] = [];
  error: boolean = false;
  

  constructor(private charactersService: CharactersService, private housesService: HousesService, private route: ActivatedRoute, private elementRouter: Router){}

  ngOnInit(){
    this.route.params.subscribe((params)=>{
      if(params['name']){
        this.loadHouseByName(params['name']);
        localStorage.setItem(this.LOAD_HOUSE_NAME, params['name']);
        this.openValue = true;
      }else{
        var loadedHouseName = localStorage.getItem(this.LOAD_HOUSE_NAME);
        if(loadedHouseName){
          this.elementRouter.navigate(['houses', loadedHouseName]);
        }else{
          this.openValue = false;
          this.currentPage = Number(localStorage.getItem(this.PAGE_NUMBER_HOUSES)) || 1;
          this.getHouses(this.currentPage);
        }
      } 
    });
  }

  getHouses(currentPage: number){
    this.housesService.getHouses(currentPage).subscribe((houses) =>{
      houses.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfHouse(value);   
      })
      this.houses = houses;
    });
  }

  setAtributesOfHouse(value: House){
    if(value.region == '') value.region = '(unknown)';
    if(value.coatOfArms == '') value.coatOfArms = '(unknown)';
    if(value.words == '') value.words = '(unknown)';
    if(value.founded == '') value.founded = '(unknown)';
    if(value.titles[0] == '') value.titles[0]='(unknown)';
    if(value.seats[0] == '') value.seats[0] = '(unknown)';


    if(value.currentLord == '') value.currentLord = '(unknown)';
    else this.charactersService.getCharacterByUrl(value.currentLord).subscribe((lord)=>{
          value.currentLord = lord.name; 
    });


    if(value.heir == '') value.heir = '(unknown)';
     else this.charactersService.getCharacterByUrl(value.heir).subscribe((heir)=>{
          value.heir = heir.name; 
    });

    if(value.founder == '') value.founder = '(unknown)';
    else this.charactersService.getCharacterByUrl(value.founder).subscribe((found)=>{
         value.founder = found.name; 
   });

    if(value.overlord == '') value.overlord = '(unknown)';
    else this.charactersService.getCharacterByUrl(value.overlord).subscribe((overlord)=>{
          value.overlord = overlord.name; 
    });

    value.swornMembers.forEach((url: string, index: number)=>{
      this.charactersService.getCharacterByUrl(url).subscribe((character)=>{
        value.swornMembers[index] = character.name;
        this.swornList[index] = character.name;
      });
    });

    if(value.cadetBranches.length == 0) value.cadetBranches[0]= '(unknown)';
    else{
      value.cadetBranches.forEach((url: string, index: number)=>{
        this.housesService.getHouseByUrl(url).subscribe((house)=>{
          value.cadetBranches[index] = house.name;
        });
      });
    }
  }

  searchHouseByName(name: string){
    if (name == '') {
      this.error=false;
      localStorage.setItem(this.LOAD_HOUSE_NAME, '');
      this.elementRouter.navigate(['houses']);
    }else{
      this.housesService.getHouseByName(name).subscribe((houses)=>{
          this.error=false;
        if(houses.length > 0){
          houses.forEach((value) => {
            if(value.name == '') value.name = "(unknown)"; 
            this.setAtributesOfHouse(value);   
          })
          localStorage.setItem(this.LOAD_HOUSE_NAME, name);
          this.elementRouter.navigate(['houses', name]);
          this.houses = houses;
          this.searchTerm = name;
        }else
            this.error = true; 
      });
    }
  }

  loadHouseByName(name: string){
    this.housesService.getHouseByName(name).subscribe((houses)=>{
      houses.forEach((value) => {
        if(value.name == "") value.name = "(unknown)"; 
        this.setAtributesOfHouse(value);   
      })
      this.houses = houses;
      this.searchTerm = name;
    });
  }

  
  clickStepPage(currentPage: number){
    this.currentPage = currentPage;
    this.getHouses(currentPage);
    localStorage.setItem(this.PAGE_NUMBER_HOUSES, currentPage.toString());
  }


  clickOnCharacter(character: string){
    this.elementRouter.navigate(['characters', character]);
  }

  searchInList(name: string, house: House){
    if(name){
      this.swornList = [];
      house.swornMembers.forEach((member)=>{
        if(member.toLowerCase().includes(name.toLowerCase()))
          this.swornList.push(member);
        if(member.toLowerCase() == name.toLowerCase()){
          this.swornList = [];
          this.swornList.push(member);
        }
      })
    }else{
      this.swornList = house.swornMembers;
    }
  }
}
