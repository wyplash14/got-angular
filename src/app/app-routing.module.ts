import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BooksComponent} from './components/books/books.component';
import {HousesComponent} from './components/houses/houses.component';
import {CharactersComponent} from "./components/characters/characters.component";

const routes: Routes = [
  {
    path: '',
    component:BooksComponent,
  },
  {
    path: 'books/:name',
    component: BooksComponent
  },
  {
    path: 'books',
    component: BooksComponent,
  },
  {
    path: 'houses/:name',
    component: HousesComponent
  }, 
  {
    path: 'houses',
    component: HousesComponent,
  },
  {
    path: 'characters/:name',
    component: CharactersComponent
  },
  {
    path: 'characters',
    component: CharactersComponent
  },
  {
    path: '**',
    component: BooksComponent,
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
