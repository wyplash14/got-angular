import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { House } from 'src/app/model/house';

@Injectable({
  providedIn: 'root'
})
export class HousesService {

  constructor(private http: HttpClient) { }

  getHouses(currentPage: number){
    return this.http.get<House[]>('https://anapioficeandfire.com/api/houses?page='+currentPage+'&pageSize=10');
  }

  getHouseByName(name: string){
    return this.http.get<House[]>('https://www.anapioficeandfire.com/api/houses?name='+name);
  }

  getHouseByUrl(url: string){
    return this.http.get<House>(url);
  }
}
