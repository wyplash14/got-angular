import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Book } from 'src/app/model/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
  constructor(private http: HttpClient) { }

  getBooks(){
    return this.http.get<Book[]>('https://anapioficeandfire.com/api/books?page=1&pageSize=12');
  }

  getBookByName(name: string){
    return this.http.get<Book[]>('https://www.anapioficeandfire.com/api/books?name='+name);
  }

  getBookByUrl(url: string){
    return this.http.get<Book>(url);
  }
}
