import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Character } from 'src/app/model/character';


@Injectable({
  providedIn: 'root'
})
export class CharactersService {
  
  constructor(private http: HttpClient) { }

  getCharacters(currentPage: number){
    return this.http.get<Character[]>('https://anapioficeandfire.com/api/characters?page='+currentPage+'&pageSize=10');
  }

  getCharacterByName(name: string){
    return this.http.get<Character[]>('https://www.anapioficeandfire.com/api/characters?name='+name);
  }

  getCharacterByUrl(url: string){
    return this.http.get<Character>(url);
  }
}
